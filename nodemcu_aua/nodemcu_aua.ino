#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <SoftwareSerial.h>

void pmsData();
void pms_response();
void pms_ap_change();
void send_data();

// PMS5003
SoftwareSerial swSer(14, 12, false, 256);
long pmcf10=0;
long pmcf25=0;
long pmcf100=0;
long pmat10=0;
long pmat25=0;
long pmat100=0;
char buf[50];
// PMS5003

String response = "";

// Конфигурация
char* ssid = "WiFi_AP_NAME"; // имя wifi сети
char* password = "WiFi_password";  // пароль от wifi сети
String lat = "45.999"; // долгота (координаты сенсора).
String lon = "45.8888"; // широта (координаты сенсора).
String city = "almaty"; // город
String token = "";


void setup() {
  swSer.begin(9600);
  pinMode(16, OUTPUT);  //led pin 16
  Serial.begin(9600);
  delay(100);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

unsigned long timer = millis();

void loop(){
  if(millis() - timer > 600000){
    timer = millis();
    pmsData();
    send_data();
    }
}


void send_data(){

  const char* host = "185.146.3.114";

  WiFiClient client;
  delay(50);
  if (!client.connect(host, 80)) {
    Serial.println("connection failed");
    return;
  }

  String data = "pmcf10=" + String(pmcf10) +
                "&pmcf25=" + String(pmcf25) +
                "&pmcf100=" + String(pmcf100) +
                "&pmat10=" + String(pmat10) +
                "&pmat25=" + String(pmat25) +
                "&pmat100=" + String(pmat100) +
                "&lat=" + lat +
                "&lon=" + lon +
                "&city=" + city +
                "&token=" + token;

  Serial.print("Requesting POST: ");
  // Send request to the server:
  client.println("POST /api/pms HTTP/1.1");
  client.println("Host: 185.146.3.114");
  client.println("Content-Type: application/x-www-form-urlencoded");
  client.println("Cache-Control: no-cache");
  client.println("Accept: */*");
  client.print("Content-Length: ");
  client.println(data.length());
  client.println();
  client.print(data);
  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
    Serial.println(line);
  }
  Serial.println("=========Server answer===========");
  String line = client.readStringUntil('\n');
  Serial.println(line);
  if (client.connected()) {
    client.stop();  // DISCONNECT FROM THE SERVER
    Serial.println("gracefully disconnected from server");
  }
}

void pmsData(){
  int count = 0;
  unsigned char c;
  unsigned char high;

  while (swSer.available()) {
    c = swSer.read();
    if((count==0 && c!=0x42) || (count==1 && c!=0x4d)){
      Serial.println("check failed");
      break;
    }
    if(count > 15){
      Serial.println("complete");
      break;
    }
    else if(count == 4 || count == 6 || count == 8 || count == 10 || count == 12 || count == 14) {high = c;}
    else if(count == 5) pmcf10 = 256*high + c;
    else if(count == 7) pmcf25 = 256*high + c;
    else if(count == 9) pmcf100 = 256*high + c;
    else if(count == 11) pmat10 = 256*high + c;
    else if(count == 13) pmat25 = 256*high + c;
    else if(count == 15) pmat100 = 256*high + c;
    count++;
  }

  while(swSer.available()) swSer.read();
  swSer.println();
}


