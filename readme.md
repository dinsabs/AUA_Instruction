# Инструкция пользователя для настройки сенсора загрязнения воздуха
# Соединения

**PMS5003** | **NodeMCU**
--- | ---
`PIN 5` | `PIN D5 (GPIO 14)`
`PIN 4` | `PIN D6 (GPIO 12)`
`PIN 1` | `PIN 3V3`
`PIN 2` | `PIN GND`


![nodemcu](nodemcu.jpg "nodemcu")
<br>
![pms_pinout](pms_pinout.jpg "pms_pinout")


# ПРОШИВКА
## 1. Скачиваем среду для программирования тут (под вашу ОС) [arduino ide]
![screen0](https://i.imgur.com/pRxSw3q.png "screen0")
<br>

## 2. Скачиваем и устанавливаем нужные пакеты
#### - __Заходим в File --> Preferences и вставляем ссылку__ 
__`http://arduino.esp8266.com/stable/package_esp8266com_index.json`__

![img](http://www.whatimade.today/content/images/2015/11/ArduinoESP1.jpg  "img")

#### - __Заходим в Tools --> Board --> Boards manager__
 
![img](http://www.whatimade.today/content/images/2015/11/ArduinoESP2.jpg  "img")

#### - __В поле поиска вводим `ESP8266`, находим нужный нам пакет и нажимаем `Install`__
 
![img](http://www.whatimade.today/content/images/2015/11/ArduinoESP3.jpg  "img")

#### - __После успешной установки, выбираем нужный нам модуль `Generic ESP8266`__

![img](http://www.whatimade.today/content/images/2015/11/ArduinoESP5.jpg "img") 


## 3. Компиляция и загрузка на плату
#### - __Подключаем NodeMCU к USB разьему__

> #### не забываем указать правильный порт

![img](http://www.whatimade.today/content/images/2015/11/ArduinoESP4.jpg "img")


#### - __Записываем данные в соответсвующие переменные и загружаем код__


```javascript
25 | char* ssid = "WiFi_AP_NAME"; // имя wifi сети
26 | char* password = "WiFi_password";  // пароль от wifi сети
27 | String lat = "45.999"; // долгота (координаты сенсора).
28 | String lon = "45.8888"; // широта (координаты сенсора).
29 | String city = "almaty"; // город
30 | String token = "TOKEN_HERE"; // ключ который вам выдали при покупке
```


### *Валидные имена городов

город | кодовое имя
--- | ---
*Астана* | `astana`
*Алматы* | `almaty`


[arduino ide]: https://www.arduino.cc/en/Main/Software